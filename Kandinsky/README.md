# Kandinsky

A simple image editor that cover the next functionalities:

- Pencil
- Eraser
- Color picker
- Custom background color
- Custom Line Width
- Draw Shapes (Circle, Square, Rectangle and Ellipse)
- Draw Lines
- Fill geometric figures
- Save image, load image and make a new project