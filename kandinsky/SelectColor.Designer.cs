﻿namespace Kandinsky
{
    partial class SelectColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.redt = new System.Windows.Forms.TrackBar();
            this.greent = new System.Windows.Forms.TrackBar();
            this.redval = new System.Windows.Forms.Label();
            this.greenval = new System.Windows.Forms.Label();
            this.bluet = new System.Windows.Forms.TrackBar();
            this.alphat = new System.Windows.Forms.TrackBar();
            this.blueval = new System.Windows.Forms.Label();
            this.alphaval = new System.Windows.Forms.Label();
            this.selectedcolor = new System.Windows.Forms.PictureBox();
            this.topick = new System.Windows.Forms.PictureBox();
            this.colorpalette = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.redt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alphat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedcolor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorpalette)).BeginInit();
            this.SuspendLayout();
            // 
            // redt
            // 
            this.redt.Location = new System.Drawing.Point(12, 170);
            this.redt.Maximum = 255;
            this.redt.Name = "redt";
            this.redt.Size = new System.Drawing.Size(104, 45);
            this.redt.TabIndex = 36;
            this.redt.Scroll += new System.EventHandler(this.red_Scroll);
            // 
            // greent
            // 
            this.greent.Location = new System.Drawing.Point(12, 210);
            this.greent.Maximum = 255;
            this.greent.Name = "greent";
            this.greent.Size = new System.Drawing.Size(104, 45);
            this.greent.TabIndex = 37;
            this.greent.Scroll += new System.EventHandler(this.green_Scroll);
            // 
            // redval
            // 
            this.redval.AutoSize = true;
            this.redval.Location = new System.Drawing.Point(139, 170);
            this.redval.Name = "redval";
            this.redval.Size = new System.Drawing.Size(36, 13);
            this.redval.TabIndex = 39;
            this.redval.Text = "R:    0";
            // 
            // greenval
            // 
            this.greenval.AutoSize = true;
            this.greenval.Location = new System.Drawing.Point(142, 210);
            this.greenval.Name = "greenval";
            this.greenval.Size = new System.Drawing.Size(33, 13);
            this.greenval.TabIndex = 41;
            this.greenval.Text = "G:   0";
            // 
            // bluet
            // 
            this.bluet.Location = new System.Drawing.Point(12, 246);
            this.bluet.Maximum = 255;
            this.bluet.Name = "bluet";
            this.bluet.Size = new System.Drawing.Size(104, 45);
            this.bluet.TabIndex = 42;
            this.bluet.Scroll += new System.EventHandler(this.blue_Scroll);
            // 
            // alphat
            // 
            this.alphat.Location = new System.Drawing.Point(12, 286);
            this.alphat.Maximum = 255;
            this.alphat.Name = "alphat";
            this.alphat.Size = new System.Drawing.Size(104, 45);
            this.alphat.TabIndex = 43;
            this.alphat.Scroll += new System.EventHandler(this.alpha_Scroll);
            // 
            // blueval
            // 
            this.blueval.AutoSize = true;
            this.blueval.Location = new System.Drawing.Point(143, 246);
            this.blueval.Name = "blueval";
            this.blueval.Size = new System.Drawing.Size(32, 13);
            this.blueval.TabIndex = 45;
            this.blueval.Text = "B:   0";
            // 
            // alphaval
            // 
            this.alphaval.AutoSize = true;
            this.alphaval.Location = new System.Drawing.Point(143, 286);
            this.alphaval.Name = "alphaval";
            this.alphaval.Size = new System.Drawing.Size(32, 13);
            this.alphaval.TabIndex = 47;
            this.alphaval.Text = "A:   0";
            // 
            // selectedcolor
            // 
            this.selectedcolor.BackColor = System.Drawing.Color.Black;
            this.selectedcolor.Location = new System.Drawing.Point(12, 110);
            this.selectedcolor.Name = "selectedcolor";
            this.selectedcolor.Size = new System.Drawing.Size(40, 32);
            this.selectedcolor.TabIndex = 48;
            this.selectedcolor.TabStop = false;
            // 
            // topick
            // 
            this.topick.BackColor = System.Drawing.Color.Black;
            this.topick.Location = new System.Drawing.Point(12, 12);
            this.topick.Name = "topick";
            this.topick.Size = new System.Drawing.Size(104, 92);
            this.topick.TabIndex = 35;
            this.topick.TabStop = false;
            this.topick.Click += new System.EventHandler(this.topick_Click);
            // 
            // colorpalette
            // 
            this.colorpalette.Image = global::Kandinsky.Properties.Resources.wheel;
            this.colorpalette.Location = new System.Drawing.Point(141, 12);
            this.colorpalette.Name = "colorpalette";
            this.colorpalette.Size = new System.Drawing.Size(130, 130);
            this.colorpalette.TabIndex = 34;
            this.colorpalette.TabStop = false;
            this.colorpalette.Click += new System.EventHandler(this.colorpalette_Click);
            this.colorpalette.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ColorPalette_MouseMove);
            // 
            // SelectColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(282, 352);
            this.Controls.Add(this.selectedcolor);
            this.Controls.Add(this.alphaval);
            this.Controls.Add(this.blueval);
            this.Controls.Add(this.alphat);
            this.Controls.Add(this.bluet);
            this.Controls.Add(this.greenval);
            this.Controls.Add(this.redval);
            this.Controls.Add(this.greent);
            this.Controls.Add(this.redt);
            this.Controls.Add(this.topick);
            this.Controls.Add(this.colorpalette);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "SelectColor";
            this.Text = "Color Picker";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SelectColor_Close);
            ((System.ComponentModel.ISupportInitialize)(this.redt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alphat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedcolor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorpalette)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox colorpalette;
        private System.Windows.Forms.PictureBox topick;
        private System.Windows.Forms.TrackBar redt;
        private System.Windows.Forms.TrackBar greent;
        private System.Windows.Forms.Label redval;
        private System.Windows.Forms.Label greenval;
        private System.Windows.Forms.TrackBar bluet;
        private System.Windows.Forms.TrackBar alphat;
        private System.Windows.Forms.Label blueval;
        private System.Windows.Forms.Label alphaval;
        private System.Windows.Forms.PictureBox selectedcolor;
    }
}