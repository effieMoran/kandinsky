﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.IO;

namespace Kandinsky
{
    public partial class SelectColor : Form
    {
        public Color color { get; set; }
        Color paintcolor = Color.Black;
        public SelectColor()
        {
            InitializeComponent();
        }

        //Color picker
        private void ColorPalette_MouseMove(object sender, MouseEventArgs e)
        {

            Bitmap bmp = (Bitmap)colorpalette.Image.Clone();
     
            if (5 < e.X && e.X < 120 && 5 < e.Y && e.Y < 120) { 
                paintcolor = bmp.GetPixel(e.X, e.Y);
                redt.Value = paintcolor.R;
                greent.Value = paintcolor.G;
                bluet.Value = paintcolor.B;
                alphat.Value = paintcolor.A;
                redval.Text = "R:   " + paintcolor.R.ToString();
                greenval.Text = "G:   " + paintcolor.G.ToString();
                blueval.Text = "B:   " + paintcolor.B.ToString();
                alphaval.Text = "A:   " + paintcolor.A.ToString();
                topick.BackColor = paintcolor;
            }
        }


        private void colorpalette_Click(object sender, EventArgs e)
        {
            selectedcolor.BackColor = paintcolor;
        }

        public void SelectColor_Close(object sender, FormClosedEventArgs e)
        {
            color = new Color();
            color = (selectedcolor.BackColor);
        }

        private void green_Scroll(object sender, EventArgs e)
        {
            paintcolor = Color.FromArgb(alphat.Value, redt.Value, greent.Value, bluet.Value);
            topick.BackColor = paintcolor;
            greenval.Text = "G:   " + paintcolor.G.ToString();
        }

        private void red_Scroll(object sender, EventArgs e)
        {
            paintcolor = Color.FromArgb(alphat.Value, redt.Value, greent.Value, bluet.Value);
            topick.BackColor = paintcolor;
            redval.Text = "R:   " + paintcolor.R.ToString();
        }

        private void blue_Scroll(object sender, EventArgs e)
        {
            paintcolor = Color.FromArgb(alphat.Value, redt.Value, greent.Value, bluet.Value);
            topick.BackColor = paintcolor;
            blueval.Text = "B:   " + paintcolor.B.ToString();
        }

        private void alpha_Scroll(object sender, EventArgs e)
        {
            paintcolor = Color.FromArgb(alphat.Value, redt.Value, greent.Value, bluet.Value);
            topick.BackColor = paintcolor;
            alphaval.Text = "A:   " + paintcolor.A.ToString();
        }

        private void topick_Click(object sender, EventArgs e)
        {
            selectedcolor.BackColor = paintcolor;
        }
    }
}
